const state = () => ({
    hospitalList: [],
    matchCount: 0,
    page: 1,
    totalCount: 0
})

export default state
