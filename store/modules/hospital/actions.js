import axios from 'axios'
import apiUrls from './apiUrls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store, payload) => {
        const serviceKey = 'Pe5Q5Oj%2FDjrwqOA7utRbO5bgHqebB908iHL57wmAXVsbPK99u9Ey%2FaO%2BF2FUcbBqpO8tK5qEMqYDLWMRk0eAcA%3D%3D'
        return axios.get(`${apiUrls.DO_LIST}?serviceKey=${serviceKey}&page=${payload.page}&cond%5BorgZipaddr%3A%3ALIKE%5D=${payload.searchKeyword}`)
            .then((res) => {
                store.commit(Constants.FETCH_LIST, res.data.data)
                store.commit(Constants.FETCH_MATCH_COUNT, res.data.matchCount)
                store.commit(Constants.FETCH_PAGE, res.data.page)
                store.commit(Constants.FETCH_TOTAL_COUNT, res.data.totalCount)
            })
    }
}
