export default {
    ssr: false,
    head: {
        title: '의료기관 정보',
        htmlAttrs: {
            lang: 'ko'
        },
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },
    css: [
        'element-ui/lib/theme-chalk/index.css'
    ],
    plugins: [
        '@/plugins/element-ui',
        '@/plugins/global-store-constants'
    ],
    components: true,
    buildModules: [],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/proxy'
    ],
    axios: {
        proxy: true
    },
    proxy: {
        '/api/apnmOrg/v1': 'https://api.odcloud.kr'
    },
    build: {
        transpile: [/^element-ui/],
    }
}
